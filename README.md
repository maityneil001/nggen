# README #

Angular v1.x generator. Generates project structure for Angular 1.x.

## What is this repository for? ##

* Generate Angular 1.x project structure with Grunt enabled.
* 1.0.8

## How do I get set up? ##

* `npm install -g ngGen`
* create new project by `ngGen -n="nameOfTheProject"`, the structure will be cloned at `nameOfTheProject`
* `cd nameOfTheProject`

## Create Module and Components ##
* generate a **module** , `ngGen add -m="moduleName"`
* generate a **controller** , `ngGen add -c="controllerName"`
* generate a **service** , `ngGen add -s="serviceName"`
* generate a **component** , `ngGen add -x="componentName"`
* generate a **template** , `ngGen add -t="templateName"`
* generate a **directive** , `ngGen add -d="directiveName"`
* `ngGen -h` for help

### Who do I talk to? ###

* Repo owner or admin : nilanjan.maity@dreamztech.com

#P.S: #  The project guideline is adopted from [johnpapa](https://johnpapa.net/)